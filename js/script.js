 const urlUsers = "https://ajax.test-danit.com/api/json/users/";
 const urlPosts = "https://ajax.test-danit.com/api/json/posts/";
 const h1 = document.querySelector("h1");

 function createMarkup(title, body, name, email) {
    return `
    <btn class = "btnDel">
    <svg style="color: black" xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
        class="post-del" viewBox="0 0 16 16">
        <path class="post-del"
            d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"
            fill="black"></path>
        <path class="post-del" fill-rule="evenodd"
            d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
            fill="black"></path>
    </svg>
    </btn>
    <div class="post-content">
        <div class="post-header">
            <a href="#" class="post-user">${name}</a>
            <span class="post-user-link">${email}</span>
            <span class="timestamp">3 hours ago</span>
        </div>

        <div class="post-body">
            <h3>${title}</h3>  
            <p>${body}</p>
        </div>

        <div class="post-footer">
            <a href="#">
                <svg viewBox="0 0 24 24" aria-hidden="true"
                class="icon r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi">
                    <g>
                        <path
                            d="M1.751 10c0-4.42 3.584-8 8.005-8h4.366c4.49 0 8.129 3.64 8.129 8.13 0 2.96-1.607 5.68-4.196 7.11l-8.054 4.46v-3.69h-.067c-4.49.1-8.183-3.51-8.183-8.01zm8.005-6c-3.317 0-6.005 2.69-6.005 6 0 3.37 2.77 6.08 6.138 6.01l.351-.01h1.761v2.3l5.087-2.81c1.951-1.08 3.163-3.13 3.163-5.36 0-3.39-2.744-6.13-6.129-6.13H9.756z">
                        </path>
                    </g>
                </svg>30
            </a>

            <a href="#">
                <svg viewBox="0 0 24 24" aria-hidden="true"
                class="icon r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi">
                    <g>
                        <path
                            d="M4.5 3.88l4.432 4.14-1.364 1.46L5.5 7.55V16c0 1.1.896 2 2 2H13v2H7.5c-2.209 0-4-1.79-4-4V7.55L1.432 9.48.068 8.02 4.5 3.88zM16.5 6H11V4h5.5c2.209 0 4 1.79 4 4v8.45l2.068-1.93 1.364 1.46-4.432 4.14-4.432-4.14 1.364-1.46 2.068 1.93V8c0-1.1-.896-2-2-2z">
                        </path>
                    </g>
                </svg>50
            </a>
            <a href="#">
                <svg viewBox="0 0 24 24" aria-hidden="true"
                class="icon r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi">
                <g>
                    <path
                        d="M16.697 5.5c-1.222-.06-2.679.51-3.89 2.16l-.805 1.09-.806-1.09C9.984 6.01 8.526 5.44 7.304 5.5c-1.243.07-2.349.78-2.91 1.91-.552 1.12-.633 2.78.479 4.82 1.074 1.97 3.257 4.27 7.129 6.61 3.87-2.34 6.052-4.64 7.126-6.61 1.111-2.04 1.03-3.7.477-4.82-.561-1.13-1.666-1.84-2.908-1.91zm4.187 7.69c-1.351 2.48-4.001 5.12-8.379 7.67l-.503.3-.504-.3c-4.379-2.55-7.029-5.19-8.382-7.67-1.36-2.5-1.41-4.86-.514-6.67.887-1.79 2.647-2.91 4.601-3.01 1.651-.09 3.368.56 4.798 2.01 1.429-1.45 3.146-2.1 4.796-2.01 1.954.1 3.714 1.22 4.601 3.01.896 1.81.846 4.17-.514 6.67z">
                    </path>
                </g>
                </svg>85
            </a>
        </div>
    </div>`;
}

 async function delPost(postForDel, postID) {
    const response = await fetch(urlPosts + postID, {
        method: "DELETE",
    });
        postForDel.remove();
}

function isClickTrash(event) {
    let target = event.target;
    if (target.matches(".post-del") || target.matches(".btnDel")) {
        const postForDel = target.closest(".post");
        const postForDelId = postForDel.id;
        delPost(postForDel, postForDelId);
    }
}

class Card {
    async getParsData(url1, url2) {
        const res = await Promise.all([fetch(url1), fetch(url2)]);
        const parsed = await Promise.all(res.map((promise) => promise.json()));
        return parsed;
    }

    render(promises) {
        const [users, posts] = promises;
        const markup = users
            .flatMap((obj) => {
                const { id: mainID, name, email } = obj;
                return posts
                    .filter((post) => post.userId === mainID)
                    .map(({ title, body, id: postID }) => {
                        const div = document.createElement("div");
                        div.classList.add("post");
                        div.id = postID;
                        div.innerHTML = createMarkup(title, body, name, email, mainID);
                        return div;
                    });
            })
        h1.after(...markup);
        return markup;
    }

    listenPosts() {
        const allPosts = document.querySelectorAll(".post");
        allPosts.forEach((post) => {
            post.addEventListener("click", isClickTrash);
        });
    }
}

const card = new Card();

card
    .getParsData(urlUsers, urlPosts)
    .then((promises) => card.render(promises))
    .then((markup) => card.listenPosts(markup));